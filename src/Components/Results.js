import React from 'react';

const Results = (props) => (

<div id="weatheroutput">
 <p>Temperature in your area is {Math.round(props.main.temp - 273.15)}&deg;C with humidy of {props.main.humidity}</p> 
<img src={props.map}/>

 </div>    
 
);

export default Results;

