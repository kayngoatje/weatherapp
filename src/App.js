import React, { Component } from 'react';
import logo from './assets/logo.png';
import './App.css';
import { BeatLoader } from 'react-spinners';
import axios from 'axios';
import Results from './Components/Results';


class App extends Component {


  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      color: '#2196F3',
      weatherdata: '',
      title: 'Weather app',
      displaymsg: '',
      info: 'The app shows the weather conditions  based on  the users current GPS location',
      lati: '',
      long: '',
      showresults: false,
      imgMap: ''

    }
  }

  //LIFECYCLE COMPONENT
  componentDidMount() {

    //GET LOCATION
    navigator.geolocation.getCurrentPosition(
      (position) => {
       console.log(position);
        this.setState({lati: position.coords.latitude});
        this.setState({long: position.coords.longitude});

       
    //FETCH API data
      axios.get("http://api.openweathermap.org/data/2.5/weather?lat=" + this.state.lati + "&lon=" + this.state.long + "&APPID=8a9ce338fed85bb61b3b93e13226ebec")
      .then(res => {
                const weatherdata = res;
                this.setState({ weatherdata: weatherdata.data});
                console.log(res);
       })
      //CATCH ERROR FOR WEATHER API
      .catch(function(error){
                 console.log(error + ' failed to load weather information in your Area!')
                 document.getElementById("displaymessage").innerHTML = (error + ' failed to load weather information in your Area!'); 
       });
      },
      //ERROR SETS FOR GEO TAG
      (error) => this.setState( {displaymsg: "Unable to retrieve your location"} ),
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000}
    );

  }
   
     
  //DISPLAY WEATHER RESULTS
  showMap = () => {
    let loader_timeout = 2000;
    let imgurl = 'https:/' + '/maps.googleapis.com/maps/api/staticmap?center=' + this.state.lati + "," + this.state.long + "&zoom=13&size=300x300&sensor=false";
    
    ////Set Url
    this.setState( {imgMap: imgurl} )   
    //Load loader
    this.setState( {loading: true} ) 
    //Refresh Weather Results
    this.setState({ showresults: false });
              
              setTimeout( () => {
              //Close loader
              this.setState( {loading: false} ) 
    
              //CHANGE STATE IF GEOLOCATION NOT SUPPORTED
              if (!navigator.geolocation){  this.setState( {displaymsg: "Geolocation is not supported by your browser"} ); return; }
               
              //SHOW WEATHER RESULTS
              this.setState({ showresults: true });
             
            }, loader_timeout );

  }

  render() {
    (function(){console.log('Component rendering')})();

    return (
      <div className="App">

        <header className="App-header">
          <div className="filter">
              <img src={logo} className="App-logo" alt="logo" />

              <h1 className="App-title">{this.state.title}</h1>

              <div id="info">{this.state.info}</div>

              <div className="group">
              <div id="displaymessage">{this.state.displaymsg}</div>
                    <div className='sweet-loading'>
                      <BeatLoader color={'#2196F3'} loading={this.state.loading}/>
                    </div> 
              </div>
              <br/>
              <button onClick={this.showMap}>Find local Weather</button>
           </div>
        </header>

         { this.state.showresults === true ?
          <div>
          <Results map = {this.state.imgMap} main = {this.state.weatherdata.main}/> 
          </div>: null
         }
      </div>
    );
  }


}


export default App;
